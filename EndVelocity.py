"""
End velocity for data files
"""

import os, sys
from math import sqrt
from statistics import mean, stdev

path = "./Tracker Sessions/Datafiler"

dirs = os.listdir( path )

endvelocity_ball = {} 
endvelocity_ring = {}

list_of_velocities_ball = []
list_of_velocities_ring = []


for filename in dirs:
    f = open(path + "/" + filename)
    lines = f.readlines()

    second_to_last_frame = lines[-2]
    last_frame = lines[-1]
    
    stlframe_list = second_to_last_frame.split("\t")
    lframe_list = last_frame.split("\t")

    time1 = float(stlframe_list[0])
    time2 = float(lframe_list[0])
    
    x1 = float(stlframe_list[1])
    x2 = float(lframe_list[1])

    dx = x2 - x1

    y1 = float(stlframe_list[2])
    y2 = float(lframe_list[2])

    dy = y2 - y1

    ds = sqrt(dx**2 + dy**2) 
    dt = time2-time1

    velocity = ds/dt

    if("Ball" in filename):
        endvelocity_ball[filename] = velocity
        list_of_velocities_ball.append(velocity)

    else:
        endvelocity_ring[filename] = velocity
        list_of_velocities_ring.append(velocity)



for i in endvelocity_ball.keys():
    print("Sluttfart for ball - fil " + i + ": " + str(endvelocity_ball[i]))

for i in endvelocity_ring.keys():
    print("Sluttfart for ring - fil " + i + ": " + str(endvelocity_ring[i]))



mean_ball = mean(list_of_velocities_ball)
mean_ring = mean(list_of_velocities_ring)
std_deviation_ball = stdev(list_of_velocities_ball)
std_deviation_ring = stdev(list_of_velocities_ring)
std_error_ball = std_deviation_ball / sqrt(len(list_of_velocities_ball))
std_error_ring = std_deviation_ring / sqrt(len(list_of_velocities_ring))


print("")
print("Gjennomsnitt Ball: " + str(mean_ball))
print("Standardavvik Ball: " + str(std_deviation_ball))
print("Standardfeil Ball: " + str(std_error_ball))


print("")
print("Gjennomsnitt Ring: " + str(mean_ring))
print("Standardavvik Ring: " + str(std_deviation_ring))
print("Standardfeil Ring: " + str(std_error_ring))
    

    

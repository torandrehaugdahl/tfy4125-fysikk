# Journal Økt 3

Dato: 6. Mars 2020 kl 08:15

Deltagere: Tor Andre Haugdahl, June Furunes-Olsen, Camilla Gunnesdal Jenssen
Fraværende: Marie Sverdupsen

Går ut fra festepunkter med tre gjeldende siffer - usikkerheten ligger i det siste siffret, og tommestokkene ble svakt bøyd ved målingene.

### Oppsett

Festepunktene til banen ble målt fra markerte nullpunkt (linje på søyle) opp til midten av festepunktet til banen. Vi vil få et avvik fra målingene i tracker for høyde siden denne vil ligge oppå banen.

For hver del av forsøket tok vi utgangspunkt i at alle opptak har samme perspektiv og at kamera står i samme posisjon i forhold til banen. Med dette kan vi lage en Tracker-fil som holder på data om akser og kalibreringsstokk slik at dette forblir konstant gjennom alle datafilene.

For selve målingene benyttet vi oss av Autotracker-funksjonen i Tracker.

En målestokk på 1m ble satt inntil banen nederst - denne satte vi også opp i Tracker.

Koordinatsystemet ble satt opp med origo direkte under første festepunkt i høyde med nullpunktslinjene.

Etter trackingen var gjennomført i hvert opptak eksporterte vi dataene i .txt filer med TSV format.


### Usikkerhet

Startpunkt kan variere noe fra test til test. Mm usikkerhet.

Andre feilkilder: 
 - Tripod kan ha flyttet seg mellom opptak (trolig ikke)
 - Slippe objektet på forskjellige steder (variasjon)
 - Høyde fra markerte nullpunkt.
 - Sted på ballen autotracker-referanse blir plassert.
 - Blir ballen dyttet eller sluppet?
 - Ringen sjangler - Treffer siden på sporet
 - To personer sto for oppsett av festepunktene på banen.



### Avvik - Høyde på festepunkt vs høyde i Tracker

I første opptak av ball merket Tracker en høyde på 0,630m for første festepunkt. Vårt festepunkt (numerisk) er satt til å være 0,289

Første festepunkt: 0,289  -- Tracker: 0,306

Andre festepunkt: 0,225 -- Tracker: ~0,247 (ballen har bevegd seg noe forbi festepunktet)

Vi må etterjustere verdier fra målingene for å ta høyde for dette avviket.



### Målinger

Ball: 

- Masse: 30,5g

- Diameter: 21,95cm

Ring: 

- Masse: 13,3g

- Diameter: 49,75cm
- Indre Diameter: 42,95cm

- Tykkelse: 0,3cm

- Bredde: 1cm



Todo: Fiks signifikante siffer i Skript
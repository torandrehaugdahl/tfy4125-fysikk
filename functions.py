"""
Lab 2 - TFY4125 Våren 2020
--------------------------------------------------
Denne filen inneholder alle egenskrevne 
funksjoner for å simuleringen av rullingen langs banen.
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicSpline


class RollingSim:
    def __init__(self, festepunkter_y, spacing, datapoints=1401):
        self.start_y = festepunkter_y[0]
        self.festepunkter_y = np.asarray(festepunkter_y)
        self.spacing = spacing 

        
        self.num_datapoints = datapoints
        self.festepunkter_x = np.asarray([0,spacing,2*spacing,3*spacing,4*spacing,5*spacing,6*spacing,7*spacing])/1000
        self.max_y = 300
        
        self.xmin = 0.000
        self.xmax = 1.401
        
        self.dx = 0.001
        x = self.x = np.arange(self.xmin, self.xmax, self.dx)

        cs = self.cs = CubicSpline(self.festepunkter_x, self.festepunkter_y, bc_type='natural')
        
        self.Nx = len(self.x)
        self.y = cs(x)
        self.dy = cs(x,1)
        self.d2y = cs(x,2)

        self.beta = np.arctan(self.dy)
        self.curvature = self.d2y / ((1 + (self.dy**2))**(3/2))

    def Velocity(self, x, g, c, y0, y):
        radicand =  (2*g*(y0 - y))/(1 + c)
        return radicand**0.5

    def VelocitySetValues(self, y):
        radicand = (2*9.81*(self.start_y - y))/(1 + 2/5)
        return radicand**0.5

    def Acceleration(self, v, k):
        return v**2 * k
    
    def NormalForce(self, M, g, beta, accel):
        return M * (g * np.cos(beta) + accel)

    def FrictionForce(self, M, c, g, beta):
        return (c * M * g * np.sin(beta))/(1 + c)

    def Time(self, velocity_x, resolution=1401):
        timeArray = np.zeros(resolution)
        timeArray[0] = 0
        for i in range(1,len(timeArray)):
            timeArray[i] = timeArray[i-1] + 0.001 / (0.5 * (velocity_x[i-1] + velocity_x[i]))
        return timeArray

    def HorizontalPosition(time):
        t = self.Time(self.VelocitySetValues(self.y))
        x =  self.x
        print(t)


    
    def PlotShape(self):
        shape = plt.figure('y(x)', figsize=(12,6)) 
        plt.plot(self.x, self.y, self.festepunkter_x, self.festepunkter_y, '*')
        plt.title('Banens Form')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$y(x)$ (m)',fontsize=20)
        plt.ylim(0,0.350)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/Shape.png')
        plt.close()


    def SlopeGraph(self): 
        slopeGraph = plt.figure('Banens helningsvinkel',figsize=(12,6))
        plt.plot(self.x, self.beta*180/np.pi)
        plt.title('Helningsvinkel')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$Beta$ (Grader)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/Slope.png')
        plt.close()


    def CurvatureGraph(self):
        krumningGraf = plt.figure('Banens krumning',figsize=(12,6))
        plt.plot(self.x, self.curvature)
        plt.title('Krumning')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$K(x)$ (1/m)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/Slope.png')
        plt.close()

    def SpeedOfXGraph(self):
        fartsXGraf = plt.figure('Banens fartsGraf(x)',figsize=(12,6))
        plt.plot(self.x, self.Velocity(self.x, 9.81, 2/5, self.festepunkter_y[0], self.cs(self.x)))
        plt.title('Fart i x retning')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$v$ (m/s)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/SpeedOfX.png')
        plt.close()


    def NormalForceGraph(self):
        NormalForce = plt.figure('Banens Normalkraft',figsize=(12,6))
        plt.plot(self.x, self.NormalForce(1, 9.81, self.beta, self.Acceleration(self.VelocitySetValues(self.y), self.curvature)))
        plt.title('Normalkraft')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$N$ (N)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/NormalForce.png')
        plt.close()


    
    def FrictionGraph(self):
        Friksjonskraften = plt.figure('Banens Friksjonskraft over Normalkraft',figsize=(12,6))
        plt.plot(self.x, np.abs((self.FrictionForce(1, 2/5, 9.81, self.beta))/(self.NormalForce(1, 9.81, self.beta, self.Acceleration(self.VelocitySetValues(self.y), self.curvature)))))
        plt.title('Friksjonskraft/Normalkraft')
        plt.xlabel('$x$ (m)',fontsize=20)
        plt.ylabel('$|f/N|$',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/Friction.png')
        plt.close()

    def HorizontalPositionGraph(self):
        HorizontalPosisjon = plt.figure('Horisontal posisjon',figsize=(12,6))
        plt.plot(self.Time(self.VelocitySetValues(self.y)), self.x)
        plt.title('Horisontal Posisjon')
        plt.xlabel('$t$ (s)',fontsize=20)
        plt.ylabel('$x$ (m)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/HorizontalPosition.png')
        plt.close()
 
    def SpeedOfTGraph(self):
        FartSomFavT = plt.figure('Hastighet',figsize=(12,6))
        plt.plot(self.Time(self.VelocitySetValues(self.y)), self.Velocity(self.x, 9.81, 2/5, self.start_y, self.cs(self.x)))
        plt.title('Hastighet')
        plt.xlabel('$t$ (s)',fontsize=20)
        plt.ylabel('$v$ (m/s)',fontsize=20)
        plt.grid()
        plt.draw()
        plt.savefig('./SimPlot/SpeedOfT.png')
        plt.close()
 

 

# TFY4106/4107/4125 Fysikk våren 2020.

# Vi importerer nødvendige biblioteker:
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import CubicSpline
from functions import RollingSim

# Omregning fra mm til m:
# Faste festepunkter 
yfast = [0]*8
yfast[0] = 0.289
yfast[1] = 0.225
yfast[2] = 0.186
yfast[3] = 0.12
yfast[4] = 0.181
yfast[5] = 0.165
yfast[6] = 0.088
yfast[7] = 0.162

sim = RollingSim(yfast, 200)

#Programmet beregner deretter de 7 tredjegradspolynomene, et
#for hvert intervall mellom to nabofestepunkter.

#Plotting

sim.PlotShape()
sim.SlopeGraph()
sim.CurvatureGraph()
sim.SpeedOfXGraph()
sim.NormalForceGraph()
sim.FrictionGraph()
sim.HorizontalPositionGraph()
sim.SpeedOfTGraph()

import re
from os import listdir
import matplotlib.pyplot as plt
import numpy as np
from math import log10, floor
from functions import RollingSim
import sys
import copy

yfast = [0]*8
yfast[0] = 0.289
yfast[1] = 0.225
yfast[2] = 0.186
yfast[3] = 0.12
yfast[4] = 0.181
yfast[5] = 0.165
yfast[6] = 0.088
yfast[7] = 0.162

sim = RollingSim(yfast, 200)

# Plot data
ball_regex = re.compile('Ball[0-9]{1,2}\.tsv')
ring_regex = re.compile('Ring[0-9]{1,2}\.tsv')

# Data File Directory
data_file_directory = "./Tracker Sessions/Datafiler"

# Plot output
plot_output_directory = "./Plott"

# Possible to route plots to different directories and change filename format
ball_plot_output = "./Plott/"
ball_plot_filename = "Ball{NR}.png"

ring_plot_output = "./Plott/"
ring_plot_filename = "Ring{NR}.png"

# Other Config
significant_figures = 20 

def round_sig(original_number, sig=significant_figures):
    original_number = float(original_number)
    if original_number == 0.0: return original_number
    return round(original_number, floor(sig - log10(abs(original_number))-1))

def tsv2plotdata(filename):
    lines = []
    with open(filename, "r") as f:
        lines = f.readlines()
    
    data_lines = lines[2:]
    data_lines = list(map(lambda x: x.split("\t"), data_lines))

    t = []
    x = []
    y = []

    
    for i in range(0, len(data_lines)):
        data_lines[i] = data_lines[i][0:3]
        data_lines[i] = list(map(lambda x: round_sig(x), data_lines[i]))

    t = np.asarray(list(map(lambda d: d[0], data_lines)))
    x = np.asarray(list(map(lambda d: d[1], data_lines)))
    y = np.asarray(list(map(lambda d: d[2], data_lines)))

    return t, x, y

def vOfT(x0, x1, t0, t1):
    if t0 == t1 == -1:
        return -1

    if x0 < 0: x0 = 0
    if x1 < 0: x1 = 0

    if t1 == t0:
        return 0

    res = abs((x1 - x0) / (t1-t0))
    return res

def padPlotData( t, x, y, size, padValue=-1 ):
    # Assumes that the dimensions of t,x,y are the same
    valuesToPad = size - len(t)

    padList = [padValue] * valuesToPad
    t = np.hstack([t, padList])
    x = np.hstack([x, padList])
    y = np.hstack([y, padList])
    
    return t,x,y



def allPlots():
    ball_plots = []
    ring_plots = []
    data_files = listdir(data_file_directory)

    ball_files = list(filter(lambda x: ball_regex.search(x) != None, data_files))
    ring_files = list(filter(lambda x: ring_regex.search(x) != None, data_files))

    ball_names = list(map(lambda x: x.replace(".tsv", ""), ball_files))
    ring_names = list(map(lambda x: x.replace(".tsv", ""), ring_files))


    for i in ball_files:
        ball_plots.append(tsv2plotdata(data_file_directory + "/" + i))

    for i in ring_files:
        ring_plots.append(tsv2plotdata(data_file_directory + "/" + i))

    ball_max_t = 0
    ball_maxlen_index = 0

    ring_max_t = 0
    ring_maxlen_index = 0

    for i in range(0, len(ball_plots)):
        if len(ball_plots[i][0]) > ball_max_t:
            ball_max_t = len(ball_plots[i][0])
            ball_maxlen_index  = i

    for i in range(0, len(ball_plots)):
        t,x,y = padPlotData(ball_plots[i][0],ball_plots[i][1],ball_plots[i][2], ball_max_t)
        ball_plots[i] = (t, x, y)
    
    for i in range(0, len(ring_plots)):
        if len(ring_plots[i][0]) > ring_max_t:
            ring_max_t = len(ring_plots[i][0])
            ring_maxlen_index  = i

    for i in range(0, len(ring_plots)):
        t,x,y = padPlotData(ring_plots[i][0],ring_plots[i][1],ring_plots[i][2], ring_max_t)
        ring_plots[i] = (t, x, y)


         
    
    
    
    simultaneousPath("Ball", ball_plots, ball_max_t, ball_maxlen_index)
    simultaneousHorizontalPosition("Ball", ball_plots, ball_max_t, ball_maxlen_index)
    simultaneousHorizontalVelocity("Ball", ball_plots, ball_max_t, ball_maxlen_index)

    simultaneousPath("Ring", ring_plots, ring_max_t, ring_maxlen_index)
    simultaneousHorizontalPosition("Ring", ring_plots, ring_max_t, ring_maxlen_index)
    simultaneousHorizontalVelocity("Ring", ring_plots, ring_max_t, ring_maxlen_index)

def simultaneousHorizontalVelocity(objectName, plots, max_t, maxlen_index ):
    velocities = [[0]*max_t] * len(plots)


    t = sim.Time(sim.VelocitySetValues(sim.y))
    velocity = sim.Velocity(sim.x, 9.81, 2/5, sim.start_y, sim.cs(sim.x))


    plt.figure(objectName + " samlet horisontal hastighet", figsize=(12,3))
    plt.title(objectName + " (samlet) horisontal hastighet")
    plt.xlabel('$t$ (s)', fontsize=20)
    plt.ylabel('$v(t)$ (m/s)', fontsize=20)


    for i in range(0,len(plots)):
        time_adjust = 0
        start_time_index = 0
        for j in range(0, len(plots[i][1])):
            if plots[i][1][j] <= 0.01:
                time_adjust = plots[i][0][j]
                start_time_index = j
            else:
                break
        
        adjusted_time_array = copy.copy(plots[i][0])
        for j in range(0, max_t):
            adjusted_time_array[j] -= time_adjust

        for j in range(1, max_t):
            velocities[i][j] = vOfT(plots[i][1][j], plots[i][1][j-1], adjusted_time_array[j], adjusted_time_array[j-1])
        plt.plot(adjusted_time_array, velocities[i], '*k')
    


    plt.plot(t, velocity)
    plt.xlim(0, 1.3)
    plt.ylim(0, 2)

    plt.grid()
    plt.show()
    plt.close()


def simultaneousPath(objectName, plots, max_t, maxlen_index):    
    interpolated_y = get_sim_positional_values(plots[maxlen_index][1])
    sim_x = get_sim_horizontal_values(plots[maxlen_index][0])
    newPlot = plt.figure(objectName + " samlet bane", figsize=(12,3))

    for i in plots:
        plt.plot(i[1], i[2], '*k')

    plt.plot(sim.x, sim.y, '*b-', lw=3)
    plt.title(objectName + " (samlet) Bane")
    plt.xlim(0, 1.4)
    plt.ylim(0, 0.4)
    plt.xlabel('$x$ (m)', fontsize=20)
    plt.ylabel('$y(x)$ (m)', fontsize=20)
    plt.grid()
    #plt.show()
    plt.draw()
    plt.savefig(plot_output_directory + "/"  + objectName + "_Samlet_Bane.png")
    plt.close()



def simultaneousHorizontalPosition(objectName, plots, ball_max_t, ball_maxlen_index):
    # Adjust time to start when ball passes zero mark - alignment
    plt.figure(objectName + " (samlet) Horisontal", figsize=(12,3))
    plt.plot(sim.Time(sim.VelocitySetValues(sim.y)), sim.x, '*b-')

    for i in range(0,len(plots)):
        time_adjust = 0
        start_time_index = 0
        for j in range(0, len(plots[i][1])):
            if plots[i][1][j] <= 0.001:
                time_adjust = plots[i][0][j]
                start_time_index = j
            else:
                break
        adjusted_time_array = copy.copy(plots[i][0])
        for j in range(0, ball_max_t):
            adjusted_time_array[j] -= time_adjust
        plt.plot(adjusted_time_array, plots[i][1], '*k')

    

    plt.legend(['Eksperiment', 'Simulering (interpolert med eksperiment)'])
    plt.title(objectName + " (samlet) Horisontal Posisjon")
    plt.xlabel('$t$ (s)', fontsize=20)
    plt.ylabel('$x$ (m)', fontsize=20)
    plt.xlim(0,1.25)
    plt.ylim(0, 2)
    plt.grid()
    plt.draw()
    #plt.show()
    plt.savefig(plot_output_directory + "/" + objectName + "_HorizontalPos.png")
    plt.close()





def plotdata2plot(time, x_pos, y_pos, title):
    newPlot = plt.figure(title, figsize=(12,3))
    interpolated_y = get_sim_positional_values(x_pos)
    plt.plot(x_pos, y_pos, '*-', color='orange')
    plt.plot(sim.x, sim.y, '*-', color='lightblue')
    plt.title(title + " Bane")
    plt.xlabel('$x$ (m)', fontsize=20)
    plt.ylabel('$y(x)$ (m)', fontsize=20)
    plt.grid()
    plt.draw()
    plt.savefig(plot_output_directory + "/" + title + "_Bane.png")
    plt.close()

    velocities = np.zeros(len(time))

    for i in range(1, len(time)):
        velocities[i] = vOfT(x_pos[i], x_pos[i-1], time[i], time[i-1])


    plt.figure(title, figsize=(12,6))
    plt.plot(time, x_pos, '*-', color='orange')
    plt.plot(sim.Time(sim.VelocitySetValues(sim.y)), sim.x, '*', color='lightblue')
    plt.legend(['Eksperiment', 'Simulering (interpolert med eksperiment)'])
    plt.title(title + " Horisontal Posisjon")
    plt.xlabel('$t$ (s)', fontsize=20)
    plt.ylabel('$x$ (m)', fontsize=20)
    plt.grid()
    plt.draw()
    plt.savefig(plot_output_directory + "/" + title + "_HorizontalPos.png")
    plt.close()


    plt.figure(title, figsize=(12,6))
    plt.title(title + " Fart i x retning")
    plt.plot(time, velocities, '*', color='orange')
    plt.plot(sim.Time(sim.VelocitySetValues(sim.y)), sim.Velocity(sim.x, 9.81, 2/5, sim.start_y, sim.cs(sim.x)))

    plt.legend(['Eksperiment', 'Simulering'])
    plt.xlabel('$t$ (s)', fontsize=20)
    plt.ylabel('$v$ (m/s)', fontsize=20)
    plt.grid()
    plt.draw()
    plt.savefig(plot_output_directory + "/" + title + "_HorizontalSpeed.png")
    plt.close()

    #acceleration = np.zeros(len(time))
    #for i in range(1, len(time)):
    #    acceleration[i] = (velocities[i] - velocities[i-1]) / (time[i] - time[i-1])


    #plt.figure(title, figsize=(12,3))
    #plt.plot(time, acceleration, '*')
    #plt.title(title + " Akselerasjon")
    #plt.xlabel('$t$ (s)', fontsize=20)
    #plt.ylabel('$a$ (m/s^2)', fontsize=20)
    #plt.grid()
    #plt.draw()
    #plt.savefig(plot_output_directory + "/" + title + "_HorizontalSpeed.png")
    #plt.close()


def get_sim_positional_values(x_array):
    x = sim.x
    y = sim.y

    interpolated_x = np.zeros(len(x_array))
    interpolated_y = np.zeros(len(x_array))

    counter = 0
    x_counter = 0
    last_x = 0
    last_y = y[0]

    while x_array[x_counter] < 0:
        interpolated_x[x_counter] = 0
        interpolated_y[x_counter] = y[0]
        x_counter += 1

    last_dist = 9999
    
    while x_counter < len(x_array) and counter < len(x):
        dist = abs(x[counter] - x_array[x_counter])
        if dist > last_dist:
            #last_x <= x_array[x_counter] and x[counter] >= x_array[x_counter]
            last_dist = 9999
                        
            dx = x[counter] - last_x
            
            dy = y[counter] - last_y 


            inc = dy/dx

            dist = x_array[x_counter] - last_x

            interpolated_x[x_counter] = x_array[x_counter]
            interpolated_y[x_counter] = y[counter - 1] + inc*dist
            
            last_x = x[counter]
            last_y = y[counter]
            x_counter = x_counter + 1
        else:
            last_dist = dist
        counter = counter + 1

    return interpolated_y

def get_sim_xvelocity(time_array):
    horizontal_values = get_sim_horizontal_values(time_array)


    xvelocity = np.zeros(len(horizontal_values))

    for i in range(1, len(time_array)):
        calc = vOfT(horizontal_values[i], horizontal_values[i-1], time_array[i], time_array[i-1])
        xvelocity[i] = calc


    return xvelocity


def get_sim_horizontal_values(time_array):
    t = sim.Time(sim.VelocitySetValues(sim.y))
    x = sim.x

    # Both t and x have the same dimensions
    # Let's do a linear pass and interpolate values that fits the
    # times of the simulation vs the time of the experiment
    # Takes in an array with all time points of the simulation
    # And returns horizontal values for the times recorded to overlay
    # on the real values


    horizontal_values = np.zeros(len(time_array))
    counter = 0
    time_counter = 0
    last_t = 0

    while time_counter < len(time_array) and counter < len(t):

        if last_t <= time_array[time_counter] and t[counter] >= time_array[time_counter]:

            dt = t[counter] - last_t 
            if dt == 0:
                horizontal_values[time_counter] = x[counter - 1]
                last_t = t[counter]
                counter = counter + 1
                continue
                    

            vx = (x[counter] - x[counter-1]) / dt
            horizontal_values[time_counter] = x[counter - 1] \
                    + vx * (time_array[time_counter] - t[counter-1])
            time_counter = time_counter + 1

        last_t = t[counter]
        counter = counter + 1
        
    return horizontal_values


def main():
    data_files = listdir(data_file_directory)

    y = lambda x: ball_regex.search(x)

    ball_files = list(filter(lambda x: ball_regex.search(x) != None, data_files))
    ring_files = list(filter(lambda x: ring_regex.search(x) != None, data_files))

    ball_names = list(map(lambda x: x.replace(".tsv", ""), ball_files))
    ring_names = list(map(lambda x: x.replace(".tsv", ""), ring_files))

    for i in range(0, len(ball_files)):
        t,x,y = tsv2plotdata(data_file_directory + "/" + ball_files[i])
        plotdata2plot(t,x,y, ball_names[i])

    for i in range(0, len(ring_files)):
        t,x,y = tsv2plotdata(data_file_directory + "/" + ring_files[i])
        plotdata2plot(t,x,y, ring_names[i])

    allPlots()


main()
